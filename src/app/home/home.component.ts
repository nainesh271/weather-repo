import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'home-component',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  cityList: any = [];
  isDataAvailable: boolean = false;
  loadWeatherComp: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.cityList = [
      {city: "London", country: "uk"},
      {city: "Berlin", country: "germany"},
      {city: "Lisbon", country: "portugal"},
      {city: "Madrid", country: "spain"},
      {city: "Amsterdam", country: "netherlands"}
    ];
    this.isDataAvailable = false;
    this.loadWeatherComp = false;
    this.checkCityParams();
  }

  checkCityParams() {
    this.route.params.subscribe(params => {
      if(params['city'] && this.cityList && this.cityList.length) {
        this.loadWeatherComp = false;
        let index = this.cityList.findIndex(cityObj => cityObj.city.toLowerCase() == params['city']);
        if(index > -1) {
          this.isDataAvailable = true;
          this.router.navigateByUrl('/weather/' + this.cityList[index].city.toLowerCase() + "/" + this.cityList[index].country.toLowerCase());
        }
        else {
          this.isDataAvailable = false;
        }
      }
      else {
        this.loadWeatherComp = true;
      }
    });
  }
  
  back() {
    this.router.navigateByUrl('/weather');
  }
}
