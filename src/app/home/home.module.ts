import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherService } from '../weather/weather.service';
import { WeatherComponent } from '../weather/weather.component';
import { SealevelComponent } from '../sealevel/sealevel.component';
import { HomeComponent } from './home.component';
import { SealevelService } from '../sealevel/sealevel.service';

@NgModule({
  imports: [
    CommonModule   
  ],
  declarations: [
    HomeComponent,
    WeatherComponent,
    SealevelComponent
  ],
  providers: [
    WeatherService,
    SealevelService
  ],
  exports:[
  
  ]
})
export class HomeModule { }