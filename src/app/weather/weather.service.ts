import { Injectable } from '@angular/core';
import { HTTPService } from "../utility/http.service";
import { RestEndPoints } from "../utility/app.restendpoints";
import { RequestParam } from '../utility/request-param';

@Injectable()
export class WeatherService {

  constructor(private httpService: HTTPService) { }

  getWeatherInfo(params) { 
    let url = RestEndPoints.GET_WEATHER_INFO;
    let sendParam: RequestParam = {
      method: 'GET',
      url: url,
      queryParams: params.queryParams,
      success: params.success,
      error: params.error
    }
    this.httpService.httpRequestCall(sendParam);
  }
}

