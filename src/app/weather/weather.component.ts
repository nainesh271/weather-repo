import { Component, OnInit } from '@angular/core';
import { AppConstants } from '../utility/app.constant';
import {WeatherService} from './weather.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'weather-component',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})

export class WeatherComponent implements OnInit {

  cityList: any = [];
  cityObj: any = {};
  isDataAvailable: boolean = false;
  isLoaded: boolean = false;

  constructor(
    private weatherService: WeatherService,
    private router: Router,
    private toastr: ToastrManager
   ){
  }

  ngOnInit() {
    let that = this;
    that.cityList = [
      {city: "London", country: "uk"},
      {city: "Berlin", country: "germany"},
      {city: "Lisbon", country: "portugal"},
      {city: "Madrid", country: "spain"},
      {city: "Amsterdam", country: "netherlands"}
    ];
    this.cityObj = {};
    this.isDataAvailable = false;
    this.isLoaded = false;
    that.getWeatherInfo();
  }

  getWeatherInfo() {
    let that = this;
    that.cityList.map(cityObj => {
      that.weatherService.getWeatherInfo({
        queryParams: {
          q: cityObj.city + "," + cityObj.country,
          appid: AppConstants.API_KEY
        },
        success: (response) => {
          cityObj.weather = (response.weather && response.weather.length && response.weather[0].description) ? response.weather[0].description : "";
          cityObj.humidity = (response.main && response.main.humidity) ? response.main.humidity : "";
          cityObj.pressure = (response.main && response.main.pressure) ? response.main.pressure : "";
          cityObj.temp = (response.main && response.main.temp) ? response.main.temp : "";

          if(response.sys) {
            if(response.sys.sunrise) {
              try {
                let sunriseDate = new Date(response.sys.sunrise).toDateString();
                sunriseDate = sunriseDate.substring(sunriseDate.indexOf(" ") + 1);
                let sunriseTime = new Date(response.sys.sunrise).toTimeString();
                sunriseTime = sunriseTime.substring(0, sunriseTime.indexOf(" ") + 1);

                cityObj.sunriseTime = sunriseDate + " " + sunriseTime;
              }
              catch(e) {}
            }
            if(response.sys.sunset) {
              try {
                let sunsetDate = new Date(response.sys.sunset).toDateString();
                sunsetDate = sunsetDate.substring(sunsetDate.indexOf(" ") + 1);
                let sunsetTime = new Date(response.sys.sunset).toTimeString();
                sunsetTime = sunsetTime.substring(0, sunsetTime.indexOf(" ") + 1);

                cityObj.sunsetTime = sunsetDate + " " + sunsetTime;
              }
              catch(e) {}
            }
          }
          this.isDataAvailable = true;
          this.isLoaded = true;
        },
        error: (error) => {
          this.isDataAvailable = false;
          this.isLoaded = true;
          this.toastr.errorToastr("Some error occured while fetching Weather info", 'Oops!');
        }
      });
    });
  }

  openForecast(cityObj) {
    this.router.navigateByUrl('/weather/' + cityObj.city.toLowerCase() + "/" + cityObj.country.toLowerCase());
  }
}
