import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SealevelComponent } from './sealevel/sealevel.component';

const routes: Routes = [
  { path: '', redirectTo: 'weather', pathMatch: 'full' },
  { 
    path: 'weather', 
    component: HomeComponent,
  },
  { 
    path: 'weather/:city', 
    component: HomeComponent,
  },
  { 
    path: 'weather/:city/:country', 
    component: SealevelComponent,
  },
  { path: '**', redirectTo: 'weather' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
