import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HTTPService } from './utility/http.service';
import { HomeModule } from './home/home.module';
import { ToastrModule } from 'ng6-toastr-notifications';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HomeModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [
    HTTPService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
