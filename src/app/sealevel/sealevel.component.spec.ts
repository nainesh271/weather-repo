import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SealevelComponent } from './sealevel.component';

describe('SealevelComponent', () => {
  let component: SealevelComponent;
  let fixture: ComponentFixture<SealevelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SealevelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SealevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
