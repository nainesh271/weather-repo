import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SealevelService } from './sealevel.service';
import { AppConstants } from '../utility/app.constant';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'sealevel-component',
  templateUrl: './sealevel.component.html',
  styleUrls: ['./sealevel.component.css']
})
export class SealevelComponent implements OnInit {

  isDataAvailable: boolean = false;
  isLoaded: boolean = false;
  cityObj: any = {};

  constructor(
    private route: ActivatedRoute,
    private sealevelService: SealevelService,
    private router: Router,
    private toastr: ToastrManager
  ) { }

  ngOnInit() {
    this.isDataAvailable = false;
    this.isLoaded = false;
    this.checkCityParams();
  }

  checkCityParams() {
    this.route.params.subscribe(params => {
      if(params['city'] && params['country']) {
        this.cityObj.city = params['city'];
        this.cityObj.country = params['country'];
        this.getForecastInfo(this.cityObj.city, this.cityObj.country);
      }
      else {
        this.isDataAvailable = false;
        this.isLoaded = true;
      }
    });
  }

  getForecastInfo(city, country) {
    let that = this;
    if(city && country) {
      that.sealevelService.getForecastInfo({
        queryParams: {
          q: city + "," + country,
          appid: AppConstants.API_KEY
        },
        success: (response) => {
          if(response.list) {
            that.cityObj.seaLevels = [];
            response.list.map(data => {
              if(data.dt_txt) {
                let hours = data.dt_txt.substring(data.dt_txt.indexOf(" ") + 1, data.dt_txt.indexOf(":"));
                if(hours == "09" && data.main && data.main.sea_level) {
                  let obj = {
                    day: data.dt_txt.split(" ")[0],
                    time: data.dt_txt.split(" ")[1],
                    seaLevel: data.main.sea_level
                  };
                  that.cityObj.seaLevels.push(obj);
                }
              }
            });
          }
          that.isDataAvailable = true;
          that.isLoaded = true;
        },
        error: (error) => {
          that.isDataAvailable = false;
          that.isLoaded = true;
          this.toastr.errorToastr("Some error occured while fetching Sea level forecast", 'Oops');
        }
      });
    }
    else {
      that.isDataAvailable = false;
      that.isLoaded = true;
    }
  }

  back() {
    this.router.navigateByUrl('/weather');
  }
}
