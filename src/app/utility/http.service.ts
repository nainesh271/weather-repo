import {Injectable, OnDestroy, Inject} from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,HttpErrorResponse,HttpRequest,HttpResponse } from '@angular/common/http';
import {Observable, Subscription, Subject} from 'rxjs';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { ToastrManager } from 'ng6-toastr-notifications';

@Injectable()
export class HTTPService implements OnDestroy{
    unsubscribe$ = new Subject();
    subscription: Subscription;
    public scope;
    params: {
        body: string,
        type: string,
        url: string,
        queryParams: any,
        success: () => void,
        error: () => void
    };

    constructor(
        private http: HttpClient,
        private router: Router,
        private toastr: ToastrManager
    ) {}

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    httpRequestCall(param) {
        let that = this;
        let obj = {
            method: param.method,
            requestBody: param.requestBody,
            queryParams: param.queryParams ? param.queryParams : null,
            contentType: param.contentType,
            screenName: param.screenName,
            action: param.action,
        }
        let config : any = that.getRequestObj(obj);
        that.http.request(param.method,environment.serverURL + param.url, config)
        .subscribe(
            (data: any) => {
                let response: any;
                if((data && data.body) || (data.status && (data.status == 200 || data.status == 201 || data.status == 204))){
                    if(data.body) {
                        response = data.body;
                    } else {
                        response = {code: data.status, success: true};
                    }
                    if(param.success){
                        param.success(response);
                    }
                }
                else {
                    this.toastr.errorToastr("Some error occured in response", "Oops!");
                }
            },
            error => {
                if(param.error){
                    param.error(error);
                }
            });
    }

    private getRequestObj(params) {
        let headers = new HttpHeaders();
        if(params.action){
            headers = headers.append('action', params.action);
        }
        let temp = new HttpParams();
        if (params.queryParams !== null) {
            Object.keys(params.queryParams).map((k) => {
              temp = temp.set(k, params.queryParams[k]);
            });
        }
        let options = {
            body: params.requestBody,
            headers: headers,
            observe : 'response',
            params: temp
        };
        return options;
    }
}