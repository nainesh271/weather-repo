export class RestEndPoints {
    public static get GET_WEATHER_INFO(): string { return '/data/2.5/weather'; }
    public static get GET_FORECAST_INFO(): string { return '/data/2.5/forecast'; }
}