export interface RequestParam{
    method: string;
    url: string;
    queryParams?: any;
    contentType?: any;
    requestBody?: string;
    success?(type?: any): void;
    error?(type?: any): void;
  }