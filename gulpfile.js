var gulp = require('gulp');
var less = require('gulp-less');

var configObj = {
  bootstrapDirectory: './node_modules/bootstrap',
  publicDirectory: './src/assets/styles',
};

gulp.task('css', function() {
  return gulp.src(configObj.publicDirectory + '/app.less')
    .pipe(less({
      includePaths: [configObj.bootstrapDirectory + '/less'],
    }))
    .pipe(gulp.dest(configObj.publicDirectory))
    .pipe(less().on('error', console.error.bind(console)));
});

gulp.task('default',gulp.series('css'));
