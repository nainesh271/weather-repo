WeatherApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.5.

Development server:
-Run `npm install` first.
-Run `npm start` for a dev server & gulp. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
-Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

About App:
-The app will show the weather information of Europe's 5 city. On click of each city's info on the main page, it will route to a component which will have information about sea level of respective city for next 5 days at 9:00 hours.

-`Gulp` is used for the process of generating css file from less.

-City names are taken as static for demo purpose.

-Implementation is done using routing. So we can directly route to the Sea level info component (If the name of city in route is part of the static city list).

-The route for directly routing to sea level component is implemented. But the city names need to be from the list. Any other city name in url will show proper error.


Error Handling Implementation:
-Error handling is done with a proper toast message for following things:
i) For api failure.
ii) Routing to any city which is not part of the static list.
iii) No Internet connection.
